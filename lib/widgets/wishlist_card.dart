import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/theme.dart';

class WishLisctCard extends StatelessWidget {
  const WishLisctCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Container(
        margin: EdgeInsets.only(top: 18.sp),
        padding: EdgeInsets.only(
          top: 11.sp,
          left: 11.sp,
          bottom: 13.sp,
          right: 18.sp,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.sp),
          color: backgroundColor4,
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(12.sp),
              child: Image.asset(
                'assets/image_shoes.png',
                width: 19.1.w,
              ),
            ),
            SizedBox(
              width: 4.w,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Terrex Urban Low',
                    style: primaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: 14.sp,
                    ),
                  ),
                  Text(
                    '\$143,98',
                    style: priceTextStyle.copyWith(
                      fontSize: 12.sp,
                    ),
                  ),
                ],
              ),
            ),
            Image.asset(
              'assets/button_wishlist_blue.png',
              width: 9.5.w,
            )
          ],
        ),
      );
    });
  }
}
