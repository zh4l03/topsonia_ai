import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../theme.dart';

class LoadingButton extends StatelessWidget {
  const LoadingButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Container(
        height: 35.sp,
        width: double.infinity,
        margin: EdgeInsets.only(top: 17.sp),
        child: TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
              backgroundColor: primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.sp))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 16.sp,
                height: 16.sp,
                child: CircularProgressIndicator(
                  strokeWidth: 0.7.w,
                  valueColor: AlwaysStoppedAnimation(primaryTextColor),
                ),
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                'Loading',
                style: primaryTextStyle.copyWith(
                  fontSize: 13.sp,
                  fontWeight: medium,
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
