// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/pages/product_page.dart';
import 'package:topsonia_mobile/theme.dart';

class ProducTile extends StatelessWidget {
  final ProductModel product;
  const ProducTile(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ProductPage(product),
            ),
          );
        },
        child: Container(
          margin: EdgeInsets.only(
            left: 30.sp,
            right: 30.sp,
          ),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(25.sp),
                child: Image.network(
                  '${urlFile}foto_produk/${product.gambar.toString().split(';').first}',
                  errorBuilder: (context, error, stackTrace) {
                    print(error);
                    return Image.asset('assets/no_image.png');
                  },
                  width: 75.w,
                  height: 55.h,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                width: 12.sp,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 3.sp,
                    ),
                    Text(
                      '${product.namaProduk}',
                      overflow: TextOverflow.ellipsis,
                      style: primaryTextStyle.copyWith(
                        fontSize: 18.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    SizedBox(
                      height: 3.sp,
                    ),
                    Text(
                      'Rp.${rupiah(product.hargaProduk.toString())}',
                      style: priceTextStyle.copyWith(
                        fontSize: 17.sp,
                        fontWeight: medium,
                      ),
                    ),
                    SizedBox(
                      height: 3.sp,
                    ),
                    Text(
                      'Stock ${product.stock} ${product.satuan}',
                      style: primaryTextStyle.copyWith(
                        fontSize: 16.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    SizedBox(
                      height: 3.sp,
                    ),
                    int.parse(product.terjual.toString()) > 0
                        ? Text(
                            'Terjual ${product.terjual.toString()}',
                            style: primaryTextStyle.copyWith(
                              fontSize: 15.sp,
                              fontWeight: semiBold,
                            ),
                          )
                        : const SizedBox(),
                    int.parse(product.terjual.toString()) > 0
                        ? SizedBox(
                            height: 3.sp,
                          )
                        : const SizedBox(),
                    Text(
                      "Dki Jakarta",
                      style: primaryTextStyle.copyWith(
                        fontSize: 15.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    // Divider(
                    //   color: Colors.white,
                    // ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
