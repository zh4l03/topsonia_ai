// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:topsonia_mobile/provider/auth_provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/product_tile.dart';
import 'package:sizer/sizer.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    ProductProvider productProvider = Provider.of<ProductProvider>(context);

    final searchController = TextEditingController();

    handleSearch() async {
      await Provider.of<ProductProvider>(context, listen: false)
          .getProducts(searchProd: searchController.text, apikey: keyApi);
      setState(() {});
    }

    Widget header() {
      return Container(
        margin: EdgeInsets.only(
          top: 19.sp,
          left: 19.sp,
          right: 19.sp,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    authProvider.user.nama.toString(),
                    style: primaryTextStyle.copyWith(
                        fontSize: 15.sp, fontWeight: semiBold),
                  ),
                  Text(
                    authProvider.user.username.toString(),
                    style: subtitleTextStyle.copyWith(fontSize: 13.sp),
                  )
                ],
              ),
            ),
            Container(
              width: 11.5.w,
              height: 8.h,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('assets/image_profile.png'))),
            )
          ],
        ),
      );
    }

    Widget categories() {
      return Container(
        margin: EdgeInsets.only(top: 4.h),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(
                width: 6.w,
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                margin: EdgeInsets.only(right: 10.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.sp),
                    color: primaryColor),
                child: Text(
                  'All Shoes',
                  style: primaryTextStyle.copyWith(
                      fontSize: 10.sp, fontWeight: medium),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                margin: EdgeInsets.only(right: 10.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.sp),
                    border: Border.all(color: subtitleColor),
                    color: transparentColor),
                child: Text(
                  'Running',
                  style: subtitleTextStyle.copyWith(
                      fontSize: 10.sp, fontWeight: medium),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                margin: EdgeInsets.only(right: 10.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.sp),
                    border: Border.all(color: subtitleColor),
                    color: transparentColor),
                child: Text(
                  'Training',
                  style: subtitleTextStyle.copyWith(
                      fontSize: 10.sp, fontWeight: medium),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                margin: EdgeInsets.only(right: 10.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.sp),
                    border: Border.all(color: subtitleColor),
                    color: transparentColor),
                child: Text(
                  'BasketBall',
                  style: subtitleTextStyle.copyWith(
                      fontSize: 10.sp, fontWeight: medium),
                ),
              ),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: 10.sp, vertical: 6.sp),
                margin: EdgeInsets.only(right: 10.sp),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.sp),
                    border: Border.all(color: subtitleColor),
                    color: transparentColor),
                child: Text(
                  'Hiking',
                  style: subtitleTextStyle.copyWith(
                      fontSize: 10.sp, fontWeight: medium),
                ),
              )
            ],
          ),
        ),
      );
    }

    Widget searchProduct() {
      return Container(
        margin: EdgeInsets.only(
          top: 19.sp,
          left: 19.sp,
          right: 19.sp,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 6.5.h,
              padding: EdgeInsets.symmetric(horizontal: 2.h),
              decoration: BoxDecoration(
                color: backgroundColor2,
                borderRadius: BorderRadius.circular(9.sp),
              ),
              child: Center(
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          handleSearch();
                        });
                      },
                      child: Icon(
                        Icons.search,
                        color: Colors.indigo.shade400,
                        size: 17.sp,
                      ),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Expanded(
                      child: TextFormField(
                        onEditingComplete: () {
                          setState(() {
                            handleSearch();
                          });
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
                        controller: searchController,
                        style: primaryTextStyle.copyWith(fontSize: 12.sp),
                        decoration: InputDecoration.collapsed(
                          hintText: 'Cari Produk',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 12.sp),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget productTitle() {
      return Container(
        margin: EdgeInsets.only(top: 19.sp, left: 20.sp, right: 19.sp),
        child: Text(
          'Produk',
          style:
              primaryTextStyle.copyWith(fontSize: 15.sp, fontWeight: semiBold),
        ),
      );
    }

    Widget productNotfound() {
      return Container(
        margin: EdgeInsets.only(left: 19.sp, right: 19.sp),
        child: Text(
          "Produk Tidak Di Temukan",
          style: priceTextStyle.copyWith(fontSize: 15.sp, fontWeight: semiBold),
        ),
      );
    }

    Widget product() {
      return GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 1.67.w / 1.26.h,
        // childAspectRatio: 6 / 7,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        // ignore: prefer_const_literals_to_create_immutables
        children: productProvider.products
            .map(
              (product) => ProducTile(product),
            )
            .toList(),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return ListView(
        children: [
          authProvider.user.idKonsumen!.isNotEmpty ? header() : SizedBox(),
          categories(),
          searchProduct(),
          productTitle(),
          SizedBox(height: 2.h),
          productProvider.products.isEmpty ? productNotfound() : product(),
        ],
      );
    });
  }
}
