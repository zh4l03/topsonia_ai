import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/pages/home/chat_page.dart';
import 'package:topsonia_mobile/pages/home/home_page.dart';
import 'package:topsonia_mobile/pages/home/profile_page.dart';
import 'package:topsonia_mobile/pages/home/wishlist_page.dart';
import 'package:topsonia_mobile/provider/auth_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    Widget cartButton() {
      return SizedBox(
        height: 35.sp,
        width: 35.sp,
        child: FloatingActionButton(
          onPressed: () async {
            if (authProvider.user.idKonsumen!.isNotEmpty) {
              if (await Provider.of<DetailKeranjangProvider>(context,
                      listen: false)
                  .getDetailKeranjang(
                      token: authProvider.user.token.toString(),
                      apikey: keyApi,
                      idKonsumen: authProvider.user.idKonsumen.toString())) {
                Navigator.pushNamed(context, '/cart');
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: secondaryColor,
                    content: Text(
                      'Gagal Periksa Koneksi Internet Anda',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12.sp),
                    ),
                  ),
                );
              }
            }
          },
          child: Image.asset(
            'assets/icon_cart.png',
            width: 5.w,
          ),
          backgroundColor: secondaryColor,
        ),
      );
    }

    Widget customBottomNav() {
      return ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(20.sp)),
        child: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          notchMargin: 11.sp,
          clipBehavior: Clip.antiAlias,
          child: BottomNavigationBar(
              backgroundColor: backgroundColor4,
              currentIndex: currentIndex,
              onTap: (value) {
                setState(() {
                  currentIndex = value;
                });
              },
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_home.png',
                        width: 5.w,
                        color: currentIndex == 0
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_chat.png',
                        width: 5.w,
                        color: currentIndex == 1
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_wishlist.png',
                        width: 5.w,
                        color: currentIndex == 2
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    icon: Container(
                      margin: EdgeInsets.only(top: 8.sp, bottom: 8.sp),
                      child: Image.asset(
                        'assets/icon_profile.png',
                        width: 5.w,
                        color: currentIndex == 3
                            ? primaryColor
                            : const Color(0xff808191),
                      ),
                    ),
                    label: ''),
              ]),
        ),
      );
    }

    Widget body() {
      switch (currentIndex) {
        case 0:
          return const HomePage();
        case 1:
          return const ChatPage();
        case 2:
          return const WishlistPage();
        case 3:
          return const ProfilePage();
        default:
          return const HomePage();
      }
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor:
            currentIndex == 0 ? backgroundColor1 : backgroundColor3,
        floatingActionButton: cartButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: customBottomNav(),
        body: body(),
      );
    });
  }
}
