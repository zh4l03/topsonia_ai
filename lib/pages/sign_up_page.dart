import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/theme.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: EdgeInsets.only(top: 18.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Sign Up',
              style: primaryTextStyle.copyWith(
                fontSize: 16.sp,
                fontWeight: semiBold,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Text(
              'Register and Happy Shopping',
              style: subtitleTextStyle.copyWith(
                fontSize: 10.sp,
              ),
            )
          ],
        ),
      );
    }

    Widget nameInput() {
      return Container(
        margin: EdgeInsets.only(top: 30.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Full Name",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    Image.asset('assets/icon_name.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'Your Full Name',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget usernameInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Username",
              style: primaryTextStyle.copyWith(
                  fontSize: 10.sp, fontWeight: medium),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    Image.asset('assets/icon_username.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                          hintText: 'Your Username',
                          hintStyle:
                              subtitleTextStyle.copyWith(fontSize: 10.sp)),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget emailInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email Address",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    Image.asset('assets/icon_email.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      decoration: InputDecoration.collapsed(
                        hintText: 'Your Email Addres',
                        hintStyle: subtitleTextStyle.copyWith(fontSize: 10.sp),
                      ),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget passwordInput() {
      return Container(
        margin: EdgeInsets.only(top: 10.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Password",
              style: primaryTextStyle.copyWith(
                fontSize: 10.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 1.sp,
            ),
            Container(
              height: 6.h,
              padding: EdgeInsets.symmetric(horizontal: 12.sp),
              decoration: BoxDecoration(
                  color: backgroundColor2,
                  borderRadius: BorderRadius.circular(10.sp)),
              child: Center(
                child: Row(
                  children: [
                    Image.asset('assets/icon_password.png', width: 4.w),
                    SizedBox(
                      width: 6.sp,
                    ),
                    Expanded(
                        child: TextFormField(
                      style: primaryTextStyle.copyWith(fontSize: 10.sp),
                      obscureText: true,
                      decoration: InputDecoration.collapsed(
                        hintText: 'Your password',
                        hintStyle: subtitleTextStyle.copyWith(fontSize: 10.sp),
                      ),
                    ))
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget signUpButton() {
      return Container(
        height: 5.h,
        width: double.infinity,
        margin: EdgeInsets.only(top: 19.sp),
        child: TextButton(
          onPressed: () {
            Navigator.pushNamed(context, '/home');
          },
          style: TextButton.styleFrom(
              backgroundColor: primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.sp))),
          child: Text('Sign UP',
              style:
                  primaryTextStyle.copyWith(fontSize: 16, fontWeight: medium)),
        ),
      );
    }

    Widget footer() {
      return Container(
        margin: EdgeInsets.only(bottom: 2.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'You have an account? ',
              style: subtitleTextStyle.copyWith(fontSize: 10.sp),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/sign-in');
              },
              child: Text(
                'Sign IN',
                style: purpleTextStyle.copyWith(
                    fontSize: 10.sp, fontWeight: medium),
              ),
            ),
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: backgroundColor1,
          body: SafeArea(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 19.sp),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    header(),
                    nameInput(),
                    usernameInput(),
                    emailInput(),
                    passwordInput(),
                    signUpButton(),
                    const Spacer(),
                    footer()
                  ]),
            ),
          ));
    });
  }
}
