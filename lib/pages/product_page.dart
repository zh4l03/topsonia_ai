import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/models/global_model.dart';
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/models/user_model.dart';
import 'package:topsonia_mobile/provider/add_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/auth_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:flutter_html/flutter_html.dart';

class ProductPage extends StatefulWidget {
  final ProductModel product;

  const ProductPage(this.product, {Key? key}) : super(key: key);

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  List images = [
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png'
  ];

  List familiarShoes = [
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png',
    'assets/image_shoes.png'
  ];

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    UserModel user = authProvider.user;

    Future<void> showSuccessDialog() async {
      ErrorModel eror = await Provider.of<AddKeranjangProvider>(
        context,
        listen: false,
      ).addKeranjang(
        token: user.token.toString(),
        idProduk: widget.product.id.toString(),
        qty: '1',
        apikey: keyApi,
        idKonsumen: user.idKonsumen.toString(),
      );

      if (eror.statusCode.toString() == '200') {
        return showDialog(
          context: context,
          builder: (BuildContext context) => Container(
            width: 20.w,
            child: AlertDialog(
              backgroundColor: backgroundColor3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.sp),
              ),
              content: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          color: primaryTextColor,
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/icon_success.png',
                      width: 20.w,
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    Text(
                      'Success',
                      style: primaryTextStyle.copyWith(
                        fontSize: 14.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    Center(
                      child: Text(
                        'Berhasil Menambahkan Produk',
                        style: secondaryTextStyle.copyWith(
                          fontSize: 12.sp,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    SizedBox(
                      width: 40.w,
                      height: 30.sp,
                      child: TextButton(
                        onPressed: () async {
                          await Provider.of<DetailKeranjangProvider>(context,
                                  listen: false)
                              .getDetailKeranjang(
                                  token: user.token.toString(),
                                  apikey: keyApi,
                                  idKonsumen: user.idKonsumen.toString());
                          Navigator.pushNamed(context, '/cart');
                        },
                        style: TextButton.styleFrom(
                          backgroundColor: primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                        child: Text(
                          'Lihat Keranjang',
                          style: primaryTextStyle.copyWith(
                            fontSize: 12.sp,
                            fontWeight: medium,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: secondaryColor,
            content: Text(
              eror.message.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12.sp),
            ),
          ),
        );
      }
    }

    Widget indicator(int index) {
      return Container(
        width: currentIndex == index ? 4.w : 1.w,
        height: 0.5.h,
        margin: EdgeInsets.symmetric(horizontal: 1.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.sp),
          color: currentIndex == index ? primaryColor : const Color(0xffC4C4C4),
        ),
      );
    }

    Widget header() {
      int index = -1;
      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 12.5.sp,
              left: 19.sp,
              right: 19.sp,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    Icons.chevron_left,
                  ),
                ),
                Icon(
                  Icons.shopping_bag,
                  color: backgroundColor1,
                ),
              ],
            ),
          ),
          CarouselSlider(
            items: widget.product.gambar
                .toString()
                .split(';')
                .map(
                  (String img) => Image.network(
                    '${urlFile}foto_produk/$img',
                    width: MediaQuery.of(context).size.width,
                    height: 310,
                    fit: BoxFit.cover,
                  ),
                )
                .toList(),
            options: CarouselOptions(
                initialPage: 0,
                onPageChanged: (index, reason) {
                  setState(() {
                    currentIndex = index;
                  });
                }),
          ),
          SizedBox(
            height: 10.sp,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.product.gambar.toString().split(';').map((e) {
              index++;
              return indicator(index);
            }).toList(),
          )
        ],
      );
    }

    Widget content() {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10.sp),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.sp),
          ),
          color: backgroundColor1,
        ),
        child: Column(
          children: [
            //Note: header
            Container(
              margin: EdgeInsets.only(
                top: 19.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.product.namaProduk.toString(),
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Image.asset(
                    'assets/button_wishlist.png',
                    width: 9.w,
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 19.sp,
                right: 19.sp,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Rp.${rupiah(widget.product.hargaProduk.toString())}',
                          style: priceTextStyle.copyWith(
                            fontSize: 10.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.sp),
              width: double.infinity,
              margin: EdgeInsets.only(
                top: 12.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              decoration: BoxDecoration(
                color: backgroundColor2,
                borderRadius: BorderRadius.circular(4.sp),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Berat',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        '${widget.product.berat} Gram',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 4.sp,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Kategori',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        widget.product.kategori.toString(),
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Stok',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                        ),
                      ),
                      Text(
                        '${widget.product.stock} ${widget.product.satuan}',
                        style: primaryTextStyle.copyWith(
                          fontSize: 9.5.sp,
                          fontWeight: semiBold,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            //Note: Description
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(
                top: 10.sp,
                left: 19.sp,
                right: 19.sp,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Description',
                    style: primaryTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 10.sp,
                    ),
                  ),
                  SizedBox(
                    height: 1.sp,
                  ),
                  Html(
                    data: '<div>${widget.product.keterangan.toString()}</div>',
                    style: {
                      'div': Style(
                        color: const Color(0xff504F5E),
                      )
                    },
                  ),
                ],
              ),
            ),

            //Note : Buttons
            Container(
              width: double.infinity,
              margin: EdgeInsets.all(
                19.sp,
              ),
              child: Row(
                children: [
                  Container(
                    width: 11.5.w,
                    height: 6.2.h,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/button_chat.png',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8.sp,
                  ),
                  Expanded(
                    child: Container(
                      height: 11.5.w,
                      child: TextButton(
                        onPressed: showSuccessDialog,
                        style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9.sp),
                          ),
                          backgroundColor: primaryColor,
                        ),
                        child: Text(
                          'Add To Cart',
                          style: primaryTextStyle.copyWith(
                            fontSize: 11.sp,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
          backgroundColor: backgroundColor6,
          body: ListView(
            children: [
              header(),
              content(),
            ],
          ));
    });
  }
}
