import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';
import 'package:topsonia_mobile/widgets/cart_card.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final rupiah = NumberFormat('#,##0', 'ID');
    DetailKeranjangProvider keranjangProvider =
        Provider.of<DetailKeranjangProvider>(context);
    int sum = keranjangProvider.detailKeranjang.isEmpty
        ? 0
        : keranjangProvider.detailKeranjang
            .map((m) =>
                int.parse(m.hargaJual.toString()) *
                int.parse(m.jumlah.toString()))
            .reduce((a, b) => a + b);

    PreferredSizeWidget header() {
      return AppBar(
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushNamed(context, '/home');
            }),
        backgroundColor: backgroundColor1,
        centerTitle: true,
        title: Text(
          'Keranjang',
          style: TextStyle(fontSize: 14.sp),
        ),
        elevation: 0,
      );
    }

    Widget emptyCart() {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/icon_empty_cart.png',
              width: 17.w,
            ),
            SizedBox(
              height: 12.sp,
            ),
            Text(
              'Opss ! Keranjang Kosong',
              style: primaryTextStyle.copyWith(
                fontSize: 12.sp,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 6.sp,
            ),
            Text(
              'Cari Produk Favorit Anda',
              style: secondaryTextStyle.copyWith(
                fontSize: 10.sp,
              ),
            ),
            Container(
              width: 40.w,
              height: 26.sp,
              margin: EdgeInsets.only(
                top: 13.sp,
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                },
                style: TextButton.styleFrom(
                  backgroundColor: primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.sp),
                  ),
                ),
                child: Text(
                  'Cari Produk',
                  style: primaryTextStyle.copyWith(
                    fontSize: 10.sp,
                    fontWeight: medium,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget content() {
      return ListView(
          padding: EdgeInsets.symmetric(
            horizontal: 19.sp,
          ),
          children: keranjangProvider.detailKeranjang
              .map(
                (keranjang) => CartCard(keranjang),
              )
              .toList());
    }

    Widget customBottomNav() {
      return Container(
        height: 19.h,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: 19.sp,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Subtotal',
                    style: primaryTextStyle.copyWith(fontSize: 12.sp),
                  ),
                  Text(
                    'Rp.${rupiah.format(int.parse(sum.toString()))}',
                    style: priceTextStyle.copyWith(
                      fontSize: 12.sp,
                      fontWeight: semiBold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Divider(
              thickness: 0.3,
              color: subtitleColor,
            ),
            SizedBox(
              height: 3.5.h,
            ),
            Container(
              height: 6.h,
              margin: EdgeInsets.symmetric(
                horizontal: defaultMargin,
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/checkout');
                },
                style: TextButton.styleFrom(
                  backgroundColor: primaryColor,
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.sp,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.sp),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Continue to Checkout',
                      style: primaryTextStyle.copyWith(
                        fontSize: 12.sp,
                        fontWeight: semiBold,
                      ),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: primaryTextColor,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
        backgroundColor: backgroundColor3,
        appBar: header(),
        body:
            keranjangProvider.detailKeranjang.isEmpty ? emptyCart() : content(),
        bottomNavigationBar: keranjangProvider.detailKeranjang.isEmpty
            ? SizedBox()
            : customBottomNav(),
      );
    });
  }
}
