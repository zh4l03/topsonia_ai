// ignore_for_file: prefer_const_constructors

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';

import '../theme.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    getInit();
    //Timer(Duration(seconds: 3), () => Navigator.pushNamed(context, '/home'));
    super.initState();
  }

  getInit() async {
    await Provider.of<ProductProvider>(context, listen: false)
        .getProducts(searchProd: '', apikey: keyApi);
    Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFAB00),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/splash.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        // child: Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   crossAxisAlignment: CrossAxisAlignment.stretch,
        //   // ignore: prefer_const_literals_to_create_immutables
        //   children: [
        //     SizedBox(
        //       height: 5,
        //     ),
        //     Image.asset(
        //       'assets/logo_topsonia.png',
        //       width: 100,
        //       height: 100,
        //     ),
        //     SizedBox(
        //       height: 15,
        //     ),
        //     Image.asset(
        //       'assets/shop2.png',
        //       width: 100,
        //       height: 100,
        //     ),
        //     // SizedBox(
        //     //   height: 100,
        //     //   width: 100,
        //     //   child: CircularProgressIndicator(
        //     //     valueColor: AlwaysStoppedAnimation(Colors.orange.shade50),
        //     //   ),
        //     // ),
        //   ],
        // ),
      ),
    );
  }
}
