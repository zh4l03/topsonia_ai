import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:topsonia_mobile/pages/cart_page.dart';
import 'package:topsonia_mobile/pages/checkout_page.dart';
import 'package:topsonia_mobile/pages/checkout_success_page.dart';
import 'package:topsonia_mobile/pages/detail_chat_page.dart';
import 'package:topsonia_mobile/pages/edit_profile.dart';
import 'package:topsonia_mobile/pages/home/main_page.dart';
import 'package:topsonia_mobile/pages/sign_in_page.dart';
import 'package:topsonia_mobile/pages/sign_up_page.dart';
import 'package:topsonia_mobile/pages/splash_page.dart';
import 'package:topsonia_mobile/provider/add_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/auth_provider.dart';
import 'package:topsonia_mobile/provider/detail_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/product_provider.dart';
import 'package:topsonia_mobile/provider/remove_keranjang_provider.dart';
import 'package:topsonia_mobile/provider/update_keranjang_provider.dart';
import 'package:topsonia_mobile/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProductProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => DetailKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => AddKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RemoveKeranjangProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => UpdateKeranjangProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => const SplashPage(),
          '/sign-in': (context) => const SignInPage(),
          '/sign-up': (context) => const SignUpPage(),
          '/home': (context) => const MainPage(),
          '/detail-chat': (context) => const DetailChatPage(),
          '/edit-profile': (context) => const EditprofilPage(),
          '/cart': (context) => const CartPage(),
          '/checkout': (context) => const CheckoutPage(),
          '/checkout-success': (context) => const CheckoutSuccesPage(),
        },
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        "Hellow World",
        style: secondaryTextStyle.copyWith(fontSize: 30),
      ),
    ));
  }
}
