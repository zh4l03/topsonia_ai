class DetailKeranjangModel {
  String? id;
  String? namaProduk;
  String? namaReseller;
  String? diskon;
  String? hargaJual;
  String? jumlah;
  String? satuan;
  String? berat;
  String? gambar;

  DetailKeranjangModel({
    this.id,
    this.namaProduk,
    this.diskon,
    this.hargaJual,
    this.jumlah,
    this.satuan,
    this.namaReseller,
    this.berat,
    this.gambar,
  });

  DetailKeranjangModel.fromJson(Map<String, dynamic> json) {
    id = json['id_produk'];
    namaProduk = json['nama_produk'];
    namaReseller = json['nama_reseller'];
    diskon = json['harga_konsumen'];
    hargaJual = json['harga_jual'];
    jumlah = json['jumlah'];
    satuan = json['satuan'];
    berat = json['berat'];
    gambar = json['gambar'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'namaProduk': namaProduk,
      'namaReseller': namaReseller,
      'diskon': diskon,
      'hargaJual': hargaJual,
      'jumlah': jumlah,
      'satuan': satuan,
      'berat': berat,
      'gambar': gambar,
    };
  }
}

class UninitializedDetailKeranjangModel extends DetailKeranjangModel {}
