import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/product_model.dart';
import 'package:topsonia_mobile/theme.dart';

class ProductService {
  Future<List<ProductModel>> getProducts({
    required String searchProd,
    required String apikey,
  }) async {
    var url = '$urlApi/product/data?cari=$searchProd';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': apikey
    };

    var response = await http.get(
      Uri.parse(url),
      headers: headers,
    );
    // print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['product'];
      List<ProductModel> products = [];
      // ignore: avoid_print
      print(jsonDecode(response.body));

      for (var item in data) {
        products.add(ProductModel.fromJson(item));
      }
      return products;
    } else {
      throw Exception('Gagal Register');
    }
  }
}
