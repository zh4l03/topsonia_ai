// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:topsonia_mobile/models/user_model.dart';
import 'package:topsonia_mobile/theme.dart';

class AuthService {
  Future<UserModel> login({
    required String username,
    required String password,
    required String keyapi,
  }) async {
    var url = '$urlApi/auth/login';
    var headers = {
      'Content-Type': 'application/json',
      'Topsonia-X-Key': keyapi
    };
    var body = jsonEncode({
      'username': username,
      'password': password,
    });

    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    print(response.body);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      UserModel user = UserModel.fromJson(data);
      return user;
    } else {
      throw Exception('Gagal Login');
    }
  }
}
