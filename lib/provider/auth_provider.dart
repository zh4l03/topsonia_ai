import 'package:flutter/material.dart';
import 'package:topsonia_mobile/models/user_model.dart';
import 'package:topsonia_mobile/services/auth_service.dart';

class AuthProvider with ChangeNotifier {
  UserModel _user = UserModel.kosong();
  UserModel get user => _user;

  set user(UserModel user) {
    _user = user;
    notifyListeners();
  }
  // late UserModel _user;

  // UserModel get user => _user;

  // set user(UserModel user) {
  //   _user = user;
  //   notifyListeners();
  // }

  Future<bool> login({
    required String username,
    required String password,
    required String keyapi,
  }) async {
    try {
      UserModel user = await AuthService().login(
        keyapi: keyapi,
        username: username,
        password: password,
      );
      _user = user;
      return true;
    } catch (e) {
      return false;
    }
  }
}
