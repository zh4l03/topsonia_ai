import 'package:flutter/widgets.dart';
import 'package:topsonia_mobile/models/error_model.dart';
import 'package:topsonia_mobile/services/add_keranjang_service.dart';

class AddKeranjangProvider with ChangeNotifier {
  Future<ErrorModel> addKeranjang(
      {required String token,
      required String idProduk,
      required String qty,
      required String apikey,
      required String idKonsumen}) async {
    try {
      ErrorModel errorr = await AddKeranjangService().addkeranjangs(
        token: token,
        idProduk: idProduk,
        qty: qty,
        apikey: apikey,
        idKonsumen: idKonsumen,
      );
      return errorr;
    } catch (e) {
      ErrorModel errorr = ErrorModel.error();
      return errorr;
    }
  }
}
